import sublime
import sublime_plugin

def escape_line_ends(text):
	return text.replace("\n","\\n")
			
def get_regions_or_whole(view):
	sel = view.sel()
	if sel:					
		if len(sel) == 1 and (sel[0].a == sel[0].b):		
			return [sublime.Region(0, view.size())]
		else:
			return sel
	return [sublime.Region(0, view.size())]

def ProcessText(view, edit, func):
	sel = get_regions_or_whole(view)		
	for r in sel:					
		newstr = func(view.substr(r))
		view.replace(edit, r, newstr)

class EscapeLineEnds(sublime_plugin.TextCommand):
	def run(self, edit):		
		ProcessText(self.view, edit, escape_line_ends)		