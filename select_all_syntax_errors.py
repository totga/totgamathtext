import sublime
import sublime_plugin

#def region_intersection(region_list_a, region_list_b):


class SelectAllSyntaxErrors(sublime_plugin.TextCommand):
	def run(self, edit):		

		errorsel = self.view.find_by_selector("invalid.illegal")

		self.view.sel().clear()
		self.view.sel().add_all(errorsel)
		
