from math import *
import sys
import os
import json
import sublime
import sublime_plugin
#sys.path.append(os.path.dirname(__file__)) #hack to make sure we can access modules on the same path as this file
import ftools
from ftools import *
class AddFormula(sublime_plugin.TextCommand):	
	def on_change(self, _str):
		print(self)

	def on_done(self, _str):
		#validate here
		if not _str in self.formula:
			try:
				ftools.validate_formula(_str)
				self.formula.append(_str)
			except Exception as e:
				form_error = "error in formula: " + str(e)
				sublime.active_window().show_quick_panel([form_error], None)
				print(form_error)
		else:
			form_warning = "Formula " + _str + " already in list."
			sublime.active_window().show_quick_panel([form_warning], None)
			print(form_warning)
			
		ftools.save_formula_list(self.formula)
		#  if user cancels with Esc key, do nothing
		#  if canceled, index is returned as  -1

	def on_cancel(self, _str):
		#  if user cancels with Esc key, do nothing
		#  if canceled, index is returned as  -1
		print("Add formula canceled")

	def run(self, edit):
		print("Running apply formula plugin")
		self.formula = ftools.load_formula_list()			   
		self.view.window().show_input_panel("Add a formula", "n*2", self.on_done, self.on_change, self.on_cancel)
