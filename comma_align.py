import sys, os
import sublime, sublime_plugin

import copy

def maxOfTwoArrays(a,b):
	res = []
	la = len(a)
	lb = len(b)
	mc = min(la, lb)
	for i in range(0, mc):
		res.append(max(a[i],b[i]))
	#top off the difference
	if la > lb:
		for i in range(lb,la):
			res.append(a[i])
	elif lb > la:
		for i in range(la,lb):
			res.append(b[i])
	return res

def incrementFrom(arr, idx, incval):
	for i in range(idx, len(arr)):
		arr[i] += incval
	return arr

def createSpaces(count):
	ret = ""
	for i in range(0, count):
		ret += ' '
	return ret

def insertSpaces(str, idx, spaceCount):
	return str[:idx] + createSpaces(spaceCount) + str[idx:]

def alignText(nstr):
	lines = nstr.split('\n')
	lineCommaPositions = []
	maxLineCommaPositions = []
	for l in lines:
		idx = l.find(",")
		clineCommaPositions = []
		while idx != -1:
			clineCommaPositions.append(idx)
			idx = l.find(",", idx + 1)
		maxLineCommaPositions = maxOfTwoArrays(clineCommaPositions, maxLineCommaPositions)					
		lineCommaPositions.append(clineCommaPositions)
	
	while True:
		ret = ""
		clineCommaPositions = copy.deepcopy(lineCommaPositions)	
		largerFound = False
		for i in range(len(lines)):
			l = lines[i]
			for j in range(0, len(clineCommaPositions[i])):
				lcp = clineCommaPositions[i][j]
				if lcp < maxLineCommaPositions[j]:
					diff = maxLineCommaPositions[j] - lcp
					l = insertSpaces(l, lcp, diff)
					incrementFrom(clineCommaPositions[i], j, diff)
				elif lcp > maxLineCommaPositions[j]:
					maxLineCommaPositions[j] = lcp
					largerFound = True
					break
					#print("Larger index found")
			ret += l + "\n"
		if largerFound == False:
			return ret
					#at this point, if any lineCommaPositions > maxLineCommaPositions, start process again
	return ret

def test():	
	str = "11231,1212,12312,1231,2341\n,1231123123,1231,451512,12312,512312512322,\n,1234122,312312412,1231212,5412231,512321\n12312,123123\n1236612223123,1232312,123123,1233"
	print(str)
	print("=======")
	print(alignText(str))


class CommaAlign(sublime_plugin.TextCommand):
	def run(self, edit):
		print("Running comma align plugin")
		view = self.view
		
		if len(view.selection) > 0 and view.selection[0].size() != 0:					
			print("Found selection")
			for region in view.selection:								
				print(region)
				nstr = view.substr(region)						
				self.view.replace(edit, region, alignText(nstr))
		else:						
			region = sublime.Region(0, self.view.size()) #whole file
			nstr = view.substr(region)
			self.view.replace(edit, region, alignText(nstr))
