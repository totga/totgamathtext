import sublime
import sublime_plugin

class ShrinkSelections(sublime_plugin.TextCommand):
	def run(self, edit):		
		sel = self.view.sel()
		regions = []
		filestr = self.view.substr(sublime.Region(0, self.view.size()))
		for r in sel:					
			if r.b - r.a > 2:
				regions.append(sublime.Region(r.a + 1, r.b -1))
				
		self.view.sel().clear()
		self.view.sel().add_all(regions)
		
