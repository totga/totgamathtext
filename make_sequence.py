import sys, os
sys.path.append(os.path.dirname(__file__)) #hack to make sure we can access modules on the same path as this file
import sublime, sublime_plugin

class MakeSequence(sublime_plugin.TextCommand):
	def run(self, edit):
		print("Running make sequence plugin")
		view = self.view
		if len(view.selection) > 0:

			first = view.substr(view.selection[0])
			num = int(first)
			for region in view.selection:
				nstr = view.substr(region)
				#self.view.replace(edit, region, "Popbump")
				self.view.replace(edit, region, str(num))
				num+=1