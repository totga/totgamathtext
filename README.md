# README #
This repository contains a sublime-text 3 package for numerically manipulating selected text, e.g.: applying a formula to each selected number, creating sequences etc.
This makes use of the fact that sublime text can have multiple discrete selections - each one is treated
separately. In the future, selections may be parsed for the numbers they contain but for now, each individual selection
must contain a whole integer or real number.

# Supplied Functions #

###TotgaMathText: Increment Numbers ###

Each discrete selection containing a number is incremented by one

###TotgaMathText: Make Sequence ###

Starting with the first descrete selected number, a sequence is created over each subsequent selection.
i.e. if the number '10' is selected, the next subsequent selection will be replaced by '11' and so on.

###TotgaMathText: Apply Formula###
Apply a formula from the list to selected text, in the form 'n*2' where 'n' is the number in the selected text.
The formula are stored in a file called 'user/totga_math_text_formula.json' located in the user's package directory (find it using preferences->browse packages).
New formula can be added to this list by either manually editing the 'user/totga_math_text_formula.json' file or by using....

###TotgaMathText: Add formula###
This command allows you to add a new formula that will appear in the quick list


Currently, there does not seem to be a way to enter user specified formula using the quick menu. This appears to be a limitation of
sublime text's quick menu: the return key only has an effect when the entered text is in the supplied list.
I'm open to suggestions regarding how to remedy this problem.

### Who do I talk to? ###

* Tim Lewis: Tim@totga-games.com