from math import *
import sys
import os
import json
import sublime
import sublime_plugin
#sys.path.append(os.path.dirname(__file__)) #hack to make sure we can access modules on the same path as this file
import ftools
formula__ = []

class ApplyFormulaInner(sublime_plugin.TextCommand):
	def run(self, edit, args):		
		global formula__
		view = self.view
		index = int(args['index'])
		formula = formula__[index] 	#grab the selected formula
		if len(view.selection) > 0:
			print("Found selection")
			for region in view.selection:
				nstr = view.substr(region)
				print(nstr)
				original_numbers = ftools.extract_numbers(nstr)
				transformed_numbers = [ftools.apply_formula(num, formula) for num in original_numbers]
				newstr = ftools.replace_original_numbers(nstr, original_numbers, transformed_numbers)
				print("replacing with " + newstr)
				self.view.replace(edit, region, newstr)


class ApplyFormula(sublime_plugin.TextCommand):

	def on_highlighted(self, index):
		print(index)

	def on_done(self, index=0):
		if index == -1:
			return
		self.view.run_command("apply_formula_inner", {'args': {'index': index}})  #if we have a valid index, call sub command - we can't keep a reference to self.view so this has to invoke a seperate command
	   
	def run(self, edit):		
		global formula__
		formula__ = ftools.load_formula_list()		   
		self.view.window().show_quick_panel(formula__, self.on_done, 1, 2, self.on_highlighted)


