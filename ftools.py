#Totga Games 2015
#ftools module - shared tools for function insert

import os
from math import *
import json
import sublime

#test
#returns default path for formula file, currently in packages root
def get_formula_file_path():
	return os.path.join(sublime.packages_path(), 'User/totga_math_text_formula.json')

#load json file containing formula. If it's not found, try creating it using default set of formula
def load_formula_list():
	default_formula = ["n*2", "n*10", "n+2 * 4", "n-2 * 2", "n*5", "sin(n)", "cos(n)", "atan(n)"]	
	fpath = get_formula_file_path()
	try:
		with open(fpath) as data_file:
			print("Found user formula file:'" + fpath + "'")
			return json.load(data_file)
			
	except:		
		print("Could not find data file 'formula.json', using default and writing to user-editable file (" + fpath + ")")
		with open(fpath, 'w') as data_file:
			data_file.write(json.dumps(default_formula))
		return default_formula

#save the list of formula to standard json file
def save_formula_list(formula):
	fpath = get_formula_file_path()
	try:
		with open(fpath, 'w') as data_file:
			data_file.write(json.dumps(formula))		
	except e:
		print(str(e))
		print("Problem saving formula file to(" + fpath + ")")

def get_num(s):
	try:
		val=int(s)
		return val
	except:
		try:
			val=float(s)
			return val
		except:
			return 0			

def is_number(var):
	try:
		float(var)
		return True
	except Exception:
		return False
	return False


def is_integer(var):
	try:
		int(var)
		return True
	except Exception:
		return False
	return False


def extract_numbers(str):
	return [get_num(s) for s in str.split() if is_number(s)]

def replace_original_numbers(instr, original_numbers, new_numbers):
	idx = 0
	lastidx = 0
	outstr = ""
	for i in range(len(original_numbers)):
		origstr = str(original_numbers[i])
		idx = instr.index(origstr, idx)		
		outstr += instr[lastidx:idx]		
		outstr += str(new_numbers[i])		
		idx += len(origstr)
		lastidx = idx
	outstr += instr[idx-1:]
	return outstr


__totga_formula_result = 0  #global value required due to the way 'exec' works

def apply_formula(n, s):
	exec("global __totga_formula_result\n__totga_formula_result=" + s)
	return str(__totga_formula_result)

def validate_formula(formula_str):
	try:
		apply_formula(10, formula_str)
		return True
	except e:
		print(str(e))
		return False