import sublime
import sublime_plugin

def get_regions_or_whole(view):
	sel = view.sel()
	if sel:					
		if len(sel) == 1 and (sel[0].a == sel[0].b):		
			return [sublime.Region(0, view.size())]
		else:
			return sel
	return [sublime.Region(0, view.size())]

def get_quoted_regions(intext, region):
	inquot = False
	
	ret = []
	rng = range(0,len(intext)) if region == None else range(region.a, region.b)
	lastStartIdx = 0

	for idx in rng:
		if intext[idx] == '"':			
			if inquot == True:								
				ret.append(sublime.Region(lastStartIdx, idx+1))
				lastStartIdx = idx
			else:								
				lastStartIdx = idx
			inquot ^= True
	return ret

class SelectAllQuotedCommand(sublime_plugin.TextCommand):
	def run(self, edit):		
		sel = get_regions_or_whole(self.view)		
		regions = []
		filestr = self.view.substr(sublime.Region(0, self.view.size()))
		for r in sel:					
			regions.extend(get_quoted_regions(filestr, r))														
		self.view.sel().clear()
		self.view.sel().add_all(regions)
		
