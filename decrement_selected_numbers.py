import sys, os
import sublime, sublime_plugin

def is_number(var):	
	try:
		if int(var) == int(var):
			return True
	except Exception:				
		return False
	return False

class DecrementSelectedNumbers(sublime_plugin.TextCommand):
	def run(self, edit):
		print("Running incremement numbers plugin")
		view = self.view
		num = 0 
		if len(view.selection) > 0:					
			print("Found selection")
			for region in view.selection:								
				nstr = view.substr(region)
				print(nstr)
				if is_number(nstr):
					print("Found number")
					num = int(nstr)
					self.view.replace(edit, region, str(num - 1))
				num+=1
